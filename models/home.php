<?php

class Home
{
    
    private $pdo;
    public $title;
    public $subtitle;    
    public $page;
    public $utilidades;

    /**
     * Constructor de modelo Home
     */
    public function __construct(){
        
        $this->utilidades = new Utilidades();        
        
        $this->page = 'home';
        $this->title = '';        
        $this->subtitle = '';

        try
		{
			$this->pdo = Database::StartUp();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }

    /**
     * Listamos los articulos desde la base
     * @param int $id El ID de categoria
     */
    public function listar_articulos($id=null){
        try
		{		                                
            $stm = !empty($id) ? $this->pdo->prepare("SELECT * FROM articulo WHERE Id_categoria = ? ORDER BY Id DESC") : $this->pdo->prepare("SELECT * FROM articulo ORDER BY Id DESC");
			$stm->execute(array($id));
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
    }

    /**
     * Listar ultimo articulo publicado
     * @param int $id El ID de articulo
     */
    public function listar_ultimo_articulo($id=null){
        try
		{		                                
            $stm = !empty($id) ? $this->pdo->prepare("SELECT * FROM articulo WHERE Id_categoria = ? ORDER BY Id DESC LIMIT 1") : $this->pdo->prepare("SELECT * FROM articulo ORDER BY Id DESC LIMIT 1;");
			$stm->execute(array($id));
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
    }

    public function listar_articulos_in($array){
        try
		{		                                            
            $stm = $this->pdo->prepare("SELECT * FROM articulo WHERE Id IN ($array) ORDER BY Id DESC");            
            $stm->execute();            
            return $stm->fetchAll(PDO::FETCH_OBJ);            
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
    }

    /**
     * Obtener información sobre la palabra clave
     * @param string $palabra La palabra clave
     */
    public function obtenerPalabraClave($palabra){        
        $stm = $this->pdo->prepare("SELECT * FROM keywords WHERE keyword = ?");
		$stm->execute(array($palabra));
		return $stm->fetch(PDO::FETCH_OBJ);
    }

    public function obtenerArticulosPorPalabra($id_palabra){
        $stm = $this->pdo->prepare("SELECT Id_articulo  FROM keywordsnews WHERE Id_keyword = ?");
		$stm->execute(array($id_palabra));
		return $stm->fetchAll(PDO::FETCH_COLUMN);
    }

}