<?php

class Usuario {
    
    private $pdo;

    public $utilidades;
    public $errors;    

    public $title;
    public $subtitle;    
    public $page;

    public $fecha;
    
    public $id;
    public $nombre;
    public $apellidos;
    public $movil;
    public $fecha_nacimiento;
    public $nombre_usuario;
    public $email;
    public $contrasena;
    public $contrasena2;

    /**
     * Constructor de modelo Usuario
     */
    public function __construct(){       
                
        $this->utilidades = new Utilidades();        
                
        // Asignamos algunos datos estáticos para varias vistas
        $page = $this->utilidades->getPageName();
        $action = $this->utilidades->getPageAction();
            
        if($page == 'usuario' && $action == null || $action == 'registrar'){
            $this->page = $page;
            $this->title = 'Crear cuenta';
            $this->subtitle = 'Tu opinión importa';
        } elseif($page == 'usuario' && $action == 'editar' || $action == 'actualizar'){
            $this->title = 'Datos de usuario';
            $this->subtitle = 'Editar perfil';
        } elseif($page == 'usuario' && $action == 'login'){
            $this->title = 'Iniciar sesión';
            $this->subtitle = 'Bienvenido';
        }
            
        try
		{
			$this->pdo = Database::StartUp();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }   

    /**
     * Registramos el usuario con los datos de formulario
     */
    public function registrar($data){        

        $this->pdo->beginTransaction();

        try 
		{                

        // Campos obligatorios
        if(empty($data->nombre) || empty($data->email) || empty($data->contrasena) || empty($data->contrasena2) || empty($data->nombre_usuario)){
            throw new Exception("Los campos Nombre, E-mail, Nombre de usuario y contraseña son obligatorios.");
        }
        
        // Comprobamos si el usuario existe            
        if(self::usuarioExiste($data->nombre_usuario)){
            throw new Exception("El usuario ya existe.");
        }
        
        // Comprobamos si las contraseñas publicadas coinciden
        if($data->contrasena !== $data->contrasena2){
            throw new Exception("Las contraseñas no coinciden.");             
        }

        $stm = "INSERT INTO usuario (Nombre,Apellidos,Movil,Fecha_nacimiento,Email,Nombre_usuario,Contrasena,Timestamp,Id_rol) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		$this->pdo->prepare($stm)
		    ->execute(
			    array(
                    $data->nombre,
                    $data->apellidos,
                    $data->movil,
                    $data->fecha_nacimiento,                    
                    $data->email,
                    $data->nombre_usuario,
                    $data->contrasena,
                    date('Y-m-d H:i:s'),
                    1
                )
            );
            
        $this->id = $this->pdo->lastInsertId();
        $this->pdo->commit();
        
        return true;

        } catch (PDOException $e){
            $this->errors = 'PDO'.$e->getMessage();
            $this->pdo->rollback();
        }
         catch (Exception $e)
		{            
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Obtenemos el objeto de usuario desde la base
     * @param int $id El id recuperado desde la sesion
     */
    public function obtener($id){
        try
		{            
			$stm = $this->pdo->prepare("SELECT * FROM usuario WHERE Id = ?");
			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();        
        }
        
    }

    /**
     * Actualizar datos de usuario
     * @param object $data El objeto de usuario
     */
    public function actualizar($data){

        $this->pdo->beginTransaction();

        try 
		{
            
        // Campos obligatorios
        if(empty($data->nombre) || empty($data->email) || empty($data->contrasena) || empty($data->contrasena2) || empty($data->nombre_usuario)){
            throw new Exception("Los campos Nombre, E-mail, Nombre de usuario y contraseña son obligatorios.");
        }

        // Comprobamos si las contraseñas publicadas coinciden
        if($data->contrasena !== $data->contrasena2){
            throw new Exception("Las contraseñas no coinciden.");             
        }

        if(self::emailReservado($data->email,$data->id)){
            throw new Exception("El e-mail ya esta reservado por el otro usuario.");
        }

        if(self::nombreUsuarioReservado($data->nombre_usuario,$data->id)){
            throw new Exception("El nombre de usuario esta reservado.");
        }        

        $stm = "UPDATE usuario SET 
				        Nombre              = ?, 
						Apellidos           = ?,
                        Movil               = ?,
                        Fecha_nacimiento    = ?,                        
                        Email               = ?,
                        Nombre_usuario      = ?,
						Contrasena          = ?
				WHERE Id = ?";

			$this->pdo->prepare($stm)
			     ->execute(
				    array(
                        $data->nombre,
                        $data->apellidos,
                        $data->movil,
                        $data->fecha_nacimiento,                    
                        $data->email,
                        $data->nombre_usuario,
                        $data->contrasena,
                        $data->id
					)
                );            
                        
            $this->pdo->commit();
        
            return true;

        } catch (PDOException $e){
            $this->errors = 'PDO'.$e->getMessage();
            $this->pdo->rollback();
        }
         catch (Exception $e)
		{            
            $this->errors = $e->getMessage();            
		}
    }
    
    /**
     * Login principal
     * @param string $email El email de usuario como nombre de usuario
     * @param string $password La contraseña
     */
    public function login($nombreUsuario,$password){
        try 
		{
            
            $stm = "SELECT Id,Id_rol FROM usuario WHERE Nombre_usuario = ? AND Contrasena = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($nombreUsuario,$password));

            if($stm->rowCount() > 0){                        
                return $stm->fetch();
            }

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Comprobamos si el usuario existe en la base en el momento de registrarlo
     * @param string $email El email introducido como nombre de usuario
     */
    private function usuarioExiste($usuario){

        try
		{
            
            $stm = "SELECT Nombre_usuario FROM usuario WHERE Nombre_usuario = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($usuario));

            if($stm->rowCount() > 0){
                return true;
            }                        

        } catch (Exception $e) {            
            $this->errors = $e->getMessage();            
		}

    }

    /**
     * Comprobamos si el email esta reservado por el otro usuario. Utilizado en el momento de editar el perfil.
     */
    private function emailReservado($email,$id){
        try 
		{
            
            $stm = "SELECT Email FROM usuario WHERE Email = ? AND Id != ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($email,$id));

            if($stm->rowCount() > 0){
                return true;
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Comprobamos si el nombre de usuario esta reservado
     * @param string $nombreUsuario Username
     * @param int $id El ID de usuario
     */
    private function nombreUsuarioReservado($nombreUsuario,$id){
        try 
		{
            
            $stm = "SELECT Nombre_usuario FROM usuario WHERE Nombre_usuario = ? AND Id != ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($nombreUsuario,$id));

            if($stm->rowCount() > 0){
                return true;
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

}