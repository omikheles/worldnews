<?php

class Articulo {
    
    private $pdo;

    public $utilidades;
    public $errors;

    public $title;
    public $subtitle;    
    public $page;

    public $fecha;
    
    public $id;
    public $idCategoria;
    public $nombreAutor;



    public $nombre;
    public $apellidos;
    public $movil;
    public $fecha_nacimiento;
    public $nombre_usuario;
    public $email;
    public $contrasena;
    public $contrasena2;

    /**
     * Constructor de modelo Usuario
     */
    public function __construct(){       
        
        $this->utilidades = new Utilidades();
                
        // Asignamos algunos datos estáticos para varias vistas
        $page = $this->utilidades->getPageName();
        $action = $this->utilidades->getPageAction();
            
        if($page == 'articulo' && $action == null){
            $this->page = $page;            
            $this->subtitle = 'Articulo';
        } elseif($page == 'usuario' && $action == 'editar' || $action == 'actualizar'){
            $this->title = 'Bienvenido';
            $this->subtitle = 'Editar perfil';
        } elseif($page == 'usuario' && $action == 'login'){
            $this->title = 'Iniciar sesión';
            $this->subtitle = 'Bienvenido';
        }
            
        try
		{
			$this->pdo = Database::StartUp();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }

    /**
     * Obtenemos el objeto de articulo por el ID
     * @param int $id El id de articulo     
     */
    public function obtenerArticulo($id){

        try
		{            
			$stm = $this->pdo->prepare("SELECT * FROM articulo WHERE Id = ?");
            $stm->execute(array($id));            
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();
        }

    }

    /**
     * Obtenemos el nombre de autor por el ID de usuario de articulo
     * @param int $id El de autor
     */
    public function obtenerNombreAutor($id){
        try 
		{
            
            $stm = "SELECT Nombre FROM usuario WHERE Id = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                
                return $stm->fetchColumn();                                
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Obtener el nombre de categoria por el id
     * @param int $id El de categoria
     */
    public function obtenerNombreCategoria($id){
        try 
		{
            
            $stm = "SELECT Nombre FROM categoria WHERE Id = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                
                return $stm->fetchColumn();                                
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Comprobamos si el articulo existe por el ID
     * @param int $id El id de articulo
     */
    private function articuloExiste($id){

        try 
		{
            
            $stm = "SELECT Id FROM articulo WHERE Id = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){
                return true;
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}

    }

    

}