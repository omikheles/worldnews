<?php

class Admin {
    
    private $pdo;

    public $utilidades;
    public $errors;    

    public $title;
    public $subtitle;    
    public $page;    
    
    public $id_usuario;
    public $titulo_articulo;
    public $image_url;
    public $cuerpo_articulo;
    public $palabras_clave_articulo;
    public $categoria_articulo;
    public $id_articulo;

    /**
     * Constructor de modelo Admin
     */
    public function __construct(){       
                
        $this->utilidades = new Utilidades();    
        
        $page = $this->utilidades->getPageName();
        $action = $this->utilidades->getPageAction();               
        
        $this->page = $page;
        $this->id_usuario = isset($_SESSION['id']) ? $_SESSION['id'] : null;
        $this->title = $this->utilidades->nombreUsuario($this->id_usuario);        
        $this->subtitle = 'Bienvenido';
                    
        try
		{
			$this->pdo = Database::StartUp();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
        }
        
    }

    /**
     * Listar articulos por el ID de usuario
     * @param int $id El ID de usuario
     */
    public function listar_articulos($id=null){
        try
		{		                                            
            $stm = !empty($id) ? $this->pdo->prepare("SELECT * FROM articulo WHERE Id_usuario = ? ORDER BY Id DESC") : $this->pdo->prepare("SELECT * FROM articulo ORDER BY Id DESC");
            $stm->execute(array($id));            
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
    }

    /**
     * Listar articulos que pertenecen a editor
     * @param int $id El ID de editor
     */
    public function listar_articulos_editor($id){
        try
		{		                                            
            $stm = $this->pdo->prepare("SELECT * FROM articulo WHERE Id_editor = ? ORDER BY Id DESC");
            $stm->execute(array($id));            
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
    }    

    /**
     * Obtener el nombre de categoria por el id
     * @param int $id El de categoria
     */
    public function obtenerNombreCategoria($id){
        try 
		{
            
            $stm = "SELECT Nombre FROM categoria WHERE Id = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                
                return $stm->fetchColumn();                                
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }
    
    /**
     * Obtenemos el nombre de autor por el ID de usuario de articulo
     * @param int $id El de autor
     */
    public function obtenerNombreAutor($id){
        try 
		{
            
            $stm = "SELECT Nombre FROM usuario WHERE Id = ? LIMIT 1";
            $stm = $this->pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                
                return $stm->fetchColumn();                                
            }                        

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Crear el articulo dentro de la base
     * @param array $data Datos de articulo
     */
    public function crear($data){
        
        $this->pdo->beginTransaction();

        try {
            
            // Campos obligatorios
            if(empty($data->titulo_articulo) || empty($data->cuerpo_articulo) || empty($data->categoria_articulo)){
                throw new Exception("Los campos Título, Categoría y Cuerpo son obligatorios.");
            }    
            
            // Subimos la imagén
            $this->fileUpload();

            // Insertamos el articulo
            $stm = "INSERT INTO articulo (Id_usuario,Id_editor,Titulo,Fecha,Imagen,Palabras_clave,Cuerpo,Id_categoria) 
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            
            $this->pdo->prepare($stm)
		    ->execute(
			    array(
                    $data->id_usuario,
                    3,
                    $data->titulo_articulo,
                    date('Y-m-d H:i:s'),
                    $data->image_url,
                    $data->palabras_clave_articulo,
                    $data->cuerpo_articulo,             
                    $data->categoria_articulo
                )
            );

            // Guardamos el Id de articulo
            $this->id_articulo = $this->pdo->lastInsertId();

            // Insertamos palabra clave         
            if(!empty($data->palabras_clave_articulo)){
                foreach ($this->palabrasClave($data->palabras_clave_articulo) as $palabra) {        
                    
                    $palabra = trim(preg_replace('/\s+/',' ', $palabra));

                    if(!$this->existePalabraClave($palabra)){
                        $this->insertarPalabraClave($palabra);
                        $this->setPalabraArticulo($this->id_articulo,$this->pdo->lastInsertId());       
                    } else {
                        $this->setPalabraArticulo($this->id_articulo,$this->obtenerPalabraClave($palabra)->Id);
                    }

                }
            }                    

            $this->pdo->commit();            

            return true;
            
        } catch (PDOException $e){
            $this->errors = 'PDO'.$e->getMessage();
            $this->pdo->rollback();        
        } catch (Exception $e)
		{            
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Actualizar articulo
     */
    public function actualizar($data){        

        $this->pdo->beginTransaction();

        try {
            
            // Campos obligatorios
            if(empty($data->titulo_articulo) || empty($data->cuerpo_articulo) || empty($data->categoria_articulo)){
                throw new Exception("Los campos Título, Categoría y Cuerpo son obligatorios.");
            }    
            
            // Subimos la imagén
            $this->fileUpload();                        

            $stm = "UPDATE articulo SET 				                                
                        Titulo          = ?,                        
                        Imagen          = ?,
                        Palabras_clave  = ?,
                        Cuerpo          = ?,
                        Id_categoria    = ?
                WHERE Id = ?";                        

            $this->pdo->prepare($stm)
			     ->execute(
				    array(
                        $data->titulo_articulo,
                        $data->image_url,
                        $data->palabras_clave_articulo,
                        $data->cuerpo_articulo,                    
                        $data->categoria_articulo,
                        $data->id_articulo                        
					)
                );
            
            // Insertamos palabra clave         
            if(!empty($data->palabras_clave_articulo)){
                
                $this->eliminarPalabraArticulo($data->id_articulo);

                foreach ($this->palabrasClave($data->palabras_clave_articulo) as $palabra) {

                    $palabra = trim(preg_replace('/\s+/',' ', $palabra));
                    
                    if(!$this->existePalabraClave($palabra)){
                        $this->insertarPalabraClave($palabra);
                        $this->setPalabraArticulo($data->id_articulo,$this->pdo->lastInsertId());       
                    } else {
                        $this->setPalabraArticulo($data->id_articulo,$this->obtenerPalabraClave($palabra)->Id);
                    }

                }
            }

            $this->pdo->commit();

            return true;

        } catch (PDOException $e){
            $this->errors = 'PDO'.$e->getMessage();
            $this->pdo->rollback();        
        } catch (Exception $e)
		{            
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Obtener el objecto de articulo desde la base
     * @param int $id El id de articulo
     */
    public function obtenerArticulo($id){

        $tipo_usuario = Utilidades::tipoUsuario($_SESSION['id']);          
        $id_usuario = $_SESSION['id'];

        try
		{            
            if($tipo_usuario == 'Autor'){
                $stm = $this->pdo->prepare("SELECT * FROM articulo WHERE Id = ? AND Id_usuario = ?");
                $pdoArray = array($id,$id_usuario);
            } elseif ($tipo_usuario == 'Editor'){
                $stm = $this->pdo->prepare("SELECT * FROM articulo WHERE Id = ? AND Id_editor = ?");
                $pdoArray = array($id,$id_usuario);
            } elseif ($tipo_usuario == 'Administrador'){
                $stm = $this->pdo->prepare("SELECT * FROM articulo WHERE Id = ?");
                $pdoArray = array($id);
            }
			
            $stm->execute($pdoArray);
            if($stm->rowCount() > 0){
                return $stm->fetch(PDO::FETCH_OBJ);
            }
			
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();        
        }        
    }

    /**
     * Eliminar artículo
     * @param int $id El id de artículo
     */
    public function eliminar($id_articulo, $id_usuario){

        $this->pdo->beginTransaction();

        try {
            
            $tipo_usuario = Utilidades::tipoUsuario($_SESSION['id']);              
            
            $this->eliminarPalabraArticulo($id_articulo);

            if($tipo_usuario == 'Autor'){
                $stm = "DELETE FROM articulo WHERE Id = ? AND Id_usuario = ?";
                $pdoArray = array($id_articulo,$id_usuario);
            } elseif ($tipo_usuario == 'Editor'){
                $stm = "DELETE FROM articulo WHERE Id = ? AND Id_editor = ?";
                $pdoArray = array($id_articulo,$id_usuario);
            } elseif ($tipo_usuario == 'Administrador'){
                $stm = "DELETE FROM articulo WHERE Id = ?";
                $pdoArray = array($id_articulo);
            }            
            
            $this->pdo->prepare($stm)
			     ->execute($pdoArray);
            
            $this->pdo->commit();
            
            return true;            

        } catch (PDOException $e){
            $this->errors = 'PDO'.$e->getMessage();
            $this->pdo->rollback();
        } catch (Exception $e)
		{            
            $this->errors = $e->getMessage();
		}
    }
    
    /**
     * Realizamos un split de las palabras separadas por coma.
     * @param string $palabras Cadena de las palabras
     */
    public function palabrasClave($palabras){
        return explode(',', $palabras);        
    }

    /**
     * Obtener información sobre la palabra clave
     * @param string $palabra La palabra clave
     */
    public function obtenerPalabraClave($palabra){
        $stm = $this->pdo->prepare("SELECT * FROM keywords WHERE keyword = ?");
		$stm->execute(array($palabra));
		return $stm->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Insertat palabra clave
     * @param string $palabra La palabra clave
     */
    public function insertarPalabraClave($palabra){
        
        $stm = "INSERT INTO keywords (keyword) VALUES (?)";

        $this->pdo->prepare($stm)
            ->execute(
                array(
                    $palabra
                )
        );

    }

    /**
     * Insertar referencia entre palabra clave y el artículo
     * @param int $id_articulo El id de artículo
     * @param int $id_palabra El id de palabra
     */
    public function setPalabraArticulo($id_articulo,$id_palabra){
        $stm = "INSERT INTO keywordsnews (Id_articulo,Id_keyword) VALUES (?,?)";
                
        $this->pdo->prepare($stm)
            ->execute(
                array(
                    $id_articulo,
                    $id_palabra    
                )
        );
    }

    /**
     * Eliminar referencia entre palabra clave y el artículo
     * @param int $id_articulo El id de articulo
     */
    public function eliminarPalabraArticulo($id_articulo){
        $stm = "DELETE FROM keywordsnews WHERE Id_articulo = ?";
                
        $this->pdo->prepare($stm)
            ->execute(
                array(
                    $id_articulo,
                )
        );
    }

    /**
     * Comprobar si palabra existe
     * @param string $palabra Palabra clave
     */
    public function existePalabraClave($palabra){            
        $stm = "SELECT * FROM keywords WHERE keyword = ? LIMIT 1";
        $stm = $this->pdo->prepare($stm);
        $stm->execute(array($palabra));

        if($stm->rowCount() > 0){
            return true;
        }        
    }

    /**
     * Subir el archivo
     */
    public function fileUpload(){        
        
        if(empty($_FILES["fichero"]["tmp_name"])){
            return false;
        }

        $target_dir = "uploads/";
        $target_file = $this->image_url = $target_dir . basename($_FILES["fichero"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));        
        
        // Comprobamos si la imagen es real.
        if(!getimagesize($_FILES["fichero"]["tmp_name"])){
            throw new Exception("Vuestra imagen parece falsa. Solo permitidos JPG, PNG. Máximo 300kb.");
        }

        // Comprobamos el tamaño máximo de la imagen.
        if ($_FILES["fichero"]["size"] > 300000) {
            throw new Exception("El fichero es demasiado grande. Máximo permitido 300kb.");            
        }

        // Únicamente permitimos los JPGs y PNGs
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            throw new Exception("El formato no es correcto. Solo permitimos los JPG, PNG.");
        }

        if (!move_uploaded_file($_FILES["fichero"]["tmp_name"], $target_file)) {
            throw new Exception("Ha ocurrido algún problema con vuestra imagen. Por favor, volver a intentar.");
        }
        
        return true;

    }

}