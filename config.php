<?php

// Iniciar session
session_start();

// Set timezone
date_default_timezone_set('Europe/Madrid');

// Definimos las variables
define('PATH', realpath(dirname(__FILE__)));
define ( 'HEADER', PATH.'/views/header.php' );
define ( 'FOOTER', PATH.'/views/footer.php' );

// Base de datos
define('DBHOST','localhost');
define('DBNAME','id9325409_p7_olegmelieli');
define('DBUSER','id9325409_olegmelieli');
define('DBPASSWORD','Grupo_OlegMeliEli2019');


// El Core
require_once(PATH.'/core/utilidades.php');
require_once(PATH.'/core/db.php');

// Enrutador
require_once(PATH.'/core/router.php');