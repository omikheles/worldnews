    <section class="pt-md-5 mt-5 mt-md-4 mb-4">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10 col-lg-8">                                  

                        <div class="form-row d-md-flex align-items-center">
                            <div class="form-group col-md-6">                                    
                                <form action="/" method="GET">
                                    <select class="form-control form-control-lg" name="filtro-categoria">
                                        <option>Filtrar por categoría</option>
                                        <option value="0">Todos</option>
                                        <?php foreach(Utilidades::listarCategorias() as $categoria): ?>
                                        <option value="<?php echo $categoria->Id; ?>"><?php echo $categoria->Nombre; ?></option>                                    
                                        <?php endforeach; ?>
                                    </select>                                                                
                                </form>
                            </div>                                                    
                            <div class="form-group col-md-6"><button id="trigger-search" class="button button--light button--light-grey button--s button--full-width d-inline-block text-center">O buscar por palabra clave<img src="/assets/img/search.svg" width="20px" alt="Buscar" class="ml-2" /></button></div>
                        </div>                                        
                        
                        <form id="category-filter" action="/" method="GET" class="d-none">
                            <div class="form-row d-flex align-items-center justify-content-end">
                                <div class="form-group col-md-8 col-lg-10">                                    
                                    <label for="keyword">Clave</label>
                                    <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Buscar por palabra clave">
                                    <small class="form-text text-muted">Ejemplo: Viaje</small>
                                </div>
                                <div class="form-group col-md-4 col-lg-2">                                                                
                                    <input type="submit" class="button button--s button--full-width d-inline-block" value="Buscar" />
                                </div>
                            </div>
                        </form>

                </div>
            </div>
        </div>
    </section>    

    <?php $count = isset($_GET['id_cat']) ? 2 : 1 ; foreach($this->model->listar_articulos($this->model->id_cat) as $articulo): ?>
    
    <?php if($count !== 1): ?>
    <article class="pt-0 pb-5 articulo">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10">    
                    <h1 class="decorate decorate--left mb-3"><a href="/articulo?id=<?php echo $articulo->Id; ?>"><?php echo $articulo->Titulo; ?></a></h1>
                    <div class="articulo__meta mb-4"><a href="/?id_cat=<?php echo $articulo->Id_categoria; ?>"><?php echo $this->articulo->obtenerNombreCategoria($articulo->Id_categoria); ?></a> <span>|</span> <?php echo $this->articulo->obtenerNombreAutor($articulo->Id_usuario); ?> <span>|</span> <?php echo $articulo->Fecha; ?></div>       
                    <?php if(!empty($articulo->Imagen)) : ?>
                    <div class="articulo__image mb-4"><img src="<?php echo $articulo->Imagen; ?>" alt="<?php echo $this->articulo->obtenerNombreCategoria($articulo->Id_categoria); ?>" class="img-full" /></div>
                    <?php endif; ?>
                    <div class="articulo__body mb-4">
                        <?php echo $this->model->utilidades->extracto($articulo->Cuerpo,100); ?>
                    </div>     
                    <div class="text-right"><a href="/articulo?id=<?php echo $articulo->Id; ?>" class="button button--s scrollto d-inline-block">Continuar leyendo</a></div>
                </div>
            </div>            
        </div>        
    </article>
    <?php endif; ?>
    
           
    <?php $count++; endforeach; ?>

    <div class="container">
        <div class="row">
            <div class="col">
                <hr class="mt-0"/>
            </div>
        </div>
    </div>

    <?php if(!$this->model->utilidades->checkLogin()): ?>
    <section class="pt-5 pb-0">
            <div class="container">
                <div class="row">
                    <div class="col text-center">                                                                                          
                        <h2 class="decorate text-center mb-5">¿Te gustaría publicar los artículos <strong>personalizados</strong>?</h2>                            
                        <p class="mb-5">¡Hola! Os invitamos a crear vuestra cuenta personal y empezar a publicar el contenido y las noticias personales. Os asignamos un editar que os ayudara en principio de vuestro camino como escritor digital.</p>          
                        <a href="/usuario" class="button scrollto d-inline-block" title="Regístrate">Regístrate</a>
                    </div>
                </div>            
            </div>        
    </section>        

    <div class="container">
        <div class="row">
            <div class="col">
                    <hr class="mt-5"/>
            </div>
        </div>
    </div>

    <?php endif; ?>

    <section class="p-5" id="contactar">
        <div class="container">
            <div class="row mb-5">
                <div class="col text-center">
                    <div class="mb-3 subtitle">¿Tienes dudas o preguntas?</div>
                    <h1 class="decorate text-center mb-5">Contactar con <b>WorldNews</b></h1>                    
                    
                    <p>Puede contactar con nosotros a través de siguiente formulario:</p>
                                        
                </div>                                
            </div>

            <div class="row">
                <div class="col">
                    <div class="text-center d-none result">
                        <img src="/assets/img/checked.svg" width="100" alt="Ok" />
                    </div>
                    <form id="contact" class="contact-form" action="/contactar" method="POST">                        
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">                                    
                            </div>
                            <div class="form-group col-md-6">     
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="comment">Mensaje</label>
                                <textarea class="form-control" id="comment" name="comment" rows="3" placeholder="Mensaje"></textarea>
                            </div>                                
                        </div>
                        <div class="form-row"> 
                            <div class="form-group col text-right">
                                <button type="submit" name="contactar" class="button">Enviar mensaje</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>