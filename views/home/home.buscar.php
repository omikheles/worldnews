<?php if($this->buscar_clave()) : ?>
    
    <section class="p-5">
        <div class="container">            
            <div class="row">
                <div class="col-12 text-center">        
                <h2>Resultados de búsqueda por clave: <strong><?php echo $this->model->utilidades->sanitize($_GET['keyword']); ?></strong></h2>
                </div>
            </div>
        </div>
    </section>

    <?php foreach((array) $this->buscar_clave() as $articulo): ?>  
    <article class="pt-0 pb-5 articulo">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10">    
                    <h2 class="decorate decorate--left mb-3"><a href="/articulo?id=<?php echo $articulo->Id; ?>"><?php echo $articulo->Titulo; ?></a></h2>
                    <div class="articulo__meta mb-4"><a href="/?id_cat=<?php echo $articulo->Id_categoria; ?>"><?php echo $this->articulo->obtenerNombreCategoria($articulo->Id_categoria); ?></a> <span>|</span> <?php echo $this->articulo->obtenerNombreAutor($articulo->Id_usuario); ?> <span>|</span> <?php echo $articulo->Fecha; ?></div>                   
                    <div class="articulo__body mb-4">
                        <?php echo $this->model->utilidades->extracto($articulo->Cuerpo,50); ?>
                    </div>     
                    <div class="text-right"><a href="/articulo?id=<?php echo $articulo->Id; ?>" class="button button--s scrollto d-inline-block">Continuar leyendo</a></div>
                </div>
            </div>            
        </div>        
    </article>

    <?php endforeach; ?>
<?php else: ?>

<section class="p-5">
    <div class="container">            
        <div class="row">
            <div class="col-12 text-center">    
                <p class="m-0">Lo sentimos. No hemos encontrado ningún resultado. <a href="/">Volver a la home.</a></p>
            </div>
        </div>
    </div>
</section>

<?php endif; ?>