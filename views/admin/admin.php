<section class="p-5">
    <div class="container">            
        <div class="row justify-content-md-center">
            <div class="col-12">            
                  
            
            <?php if(Utilidades::tipoUsuario($_SESSION['id']) == 'Autor'): ?>                                

                <!-- Autor -->

                <?php if($this->model->listar_articulos($this->model->id_usuario)) : ?>

                    <h2>Artículos publicados:</h2>
                
                    <?php foreach($this->model->listar_articulos($this->model->id_usuario) as $articulo): ?>              
                    <div class="row article-unit d-md-flex align-items-center pt-4 pb-4">
                        <div class="col-12 col-md-8 mb-3 mb-md-0">
                            <div class="article-unit__title"><a href="/articulo?id=<?php echo $articulo->Id; ?>"><?php echo $articulo->Titulo; ?></a></div>
                            <div class="article-unit__meta"><?php echo $this->model->obtenerNombreCategoria($articulo->Id_categoria) ?> | <?php echo $this->model->obtenerNombreAutor($articulo->Id_usuario) ?> | <?php echo $articulo->Fecha; ?></div>
                        </div>
                        <div class="col-12 col-md-4 text-md-right article-unit__tools">
                            <a href="/admin/editar?id=<?php echo $articulo->Id; ?>" class="mr-3 pl-3 pr-3">Editar</a><a href="/admin/eliminar?id=<?php echo $articulo->Id; ?>" class="pl-3 pr-3">Borrar</a>
                        </div>
                    </div>            
                    <?php endforeach; ?>

                <?php else: ?>
                    <div class="text-center">
                        <p class="m-0">Aun no tienes articulos publicados. <br/><a href="/admin/crear">Crear artículo.</a></p>
                    </div>
                <?php endif; ?>
            
            <?php elseif(Utilidades::tipoUsuario($_SESSION['id']) == 'Editor'): ?>

                <!-- Editor -->
                <?php if($this->model->listar_articulos_editor($_SESSION['id'])) : ?>

                    <h2>Editar los artículos:</h2>
                
                    <?php foreach($this->model->listar_articulos_editor($_SESSION['id']) as $articulo): ?>          
                                        
                    <div class="row article-unit d-md-flex align-items-center pt-4 pb-4">
                        <div class="col-12 col-md-8 mb-3 mb-md-0">
                            <div class="article-unit__title"><a href=""><?php echo $articulo->Titulo; ?></a></div>          
                            <div class="article-unit__meta"><?php echo $this->model->obtenerNombreCategoria($articulo->Id_categoria) ?> | <?php echo $this->model->obtenerNombreAutor($articulo->Id_usuario) ?> | <?php echo $articulo->Fecha; ?></div>              
                        </div>
                        <div class="col-12 col-md-4 text-md-right article-unit__tools">
                            <a href="/admin/editar?id=<?php echo $articulo->Id; ?>" class="mr-3 pl-3 pr-3">Editar</a><a href="/admin/eliminar?id=<?php echo $articulo->Id; ?>" class="pl-3 pr-3">Borrar</a>
                        </div>
                    </div>            
                    <?php endforeach; ?>

                <?php else: ?>
                    <div class="text-center">
                        <p class="m-0">Aun no hay articulos publicados. <br/><a href="/admin/crear">Crear artículo.</a></p>
                    </div>
                <?php endif; ?>

            <?php else: ?>
                
                <!-- Administrador-->

                <?php if($this->model->listar_articulos()) : ?>

                    <h2>Administrar todos los artículos publicados:</h2>
                
                    <?php foreach($this->model->listar_articulos() as $articulo): ?>                
                    <div class="row article-unit d-md-flex align-items-center pt-4 pb-4">
                        <div class="col-12 col-md-8 mb-3 mb-md-0">
                            <div class="article-unit__title"><a href=""><?php echo $articulo->Titulo; ?></a></div>          
                            <div class="article-unit__meta"><?php echo $this->model->obtenerNombreCategoria($articulo->Id_categoria) ?> | <?php echo $this->model->obtenerNombreAutor($articulo->Id_usuario) ?> | <?php echo $articulo->Fecha; ?></div>              
                        </div>
                        <div class="col-12 col-md-4 text-md-right article-unit__tools">
                            <a href="/admin/editar?id=<?php echo $articulo->Id; ?>" class="mr-3 pl-3 pr-3">Editar</a><a href="/admin/eliminar?id=<?php echo $articulo->Id; ?>" class="pl-3 pr-3">Borrar</a>
                        </div>
                    </div>            
                    <?php endforeach; ?>

                <?php else: ?>
                    <div class="text-center">
                        <p class="m-0">Aun no hay articulos publicados. <br/><a href="/admin/crear">Crear artículo.</a></p>
                    </div>
                <?php endif; ?>
                
            <?php endif; ?>

            </div>
        </div>
    </div>
</section>