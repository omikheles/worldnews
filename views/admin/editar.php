<section class="p-5">
    <div class="container">            
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-10">
                <h2>Editar artículo</h2>

                <div class="errors text-center mb-3"><?php echo $this->model->errors; ?></div>
                    <form class="admin-form-articulo" action="/admin/editar?id=<?php echo $id; ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-row">
                            <div class="form-group col-md-12">                                    
                                <input type="text" class="form-control" id="titulo" name="titulo_articulo" placeholder="Título" value="<?php echo !empty($articulo->Titulo) ? $articulo->Titulo : null; ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <div class="admin-form-articulo__image-upload p-3">
                                <label class="mr-md-2 d-inline-block">Subir una imagen destacada (máximum 300kb)</label>                                
                                <input type="file" name="fichero" id="fichero" <?php echo !empty($articulo->Imagen) ? 'required' : ''; ?>>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-8">                                    
                                <input type="text" class="form-control" id="palabras_clave_articulo" name="palabras_clave_articulo" placeholder="Conjunto de palabras clave" value="<?php echo !empty($articulo->Palabras_clave) ? $articulo->Palabras_clave : null; ?>">
                                <small class="form-text text-muted">Separados por coma</small>
                            </div>
                            <div class="form-group col-md-4">                                    
                                <select class="form-control form-control-lg" name="categoria_articulo">
                                    <option value="">Seleccionar categoría</option>                                    
                                    <?php foreach(Utilidades::listarCategorias() as $categoria): ?>
                                    <option <?php echo ($categoria->Id == $articulo->Id_categoria) ? 'selected' :  '' ?> value="<?php echo $categoria->Id; ?>"><?php echo $categoria->Nombre; ?></option>                                    
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <textarea class="form-control" id="cuerpo_articulo" name="cuerpo_articulo" rows="3" placeholder="Cuerpo de artículo"><?php echo !empty($articulo->Cuerpo) ? $articulo->Cuerpo : null; ?></textarea>
                            </div>
                        </div>
                        <div class="form-row"> 
                            <div class="form-group col text-right">                                
                                <button type="submit" name="editar" class="button">Publicar</button>
                            </div>
                        </div>                        
                    </form>

            </div>
        </div>
    </div>
</section>