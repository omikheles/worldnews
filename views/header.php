<!doctype html>
<html lang="es">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="/assets/img/favicon.svg"/>

    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Raleway:500,700" rel="stylesheet">    
    <link rel="stylesheet" href="/assets/css/main.css?v=040519">

    <title>WorldNews - Tal vez los artículos más brillantes.</title>

  </head>
  <body>

    <div class="hero<?php echo ($this->model->page == 'home') ? '' : ' hero--inner'; ?>">

        <header class="cabecera pt-2 pb-2">
            <div class="container">
                <nav class="navbar navbar-light navbar-expand-lg">
                    
                    <a class="navbar-brand mr-5" href="/"><img src="/assets/img/logo.svg" width="200px" alt="" /></a>
                    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarText">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active"><a class="nav-link" href="/">Últimas noticias</a></li>                            
                            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categorías</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <?php foreach(Utilidades::listarCategorias() as $categoria): ?>
                                    <a class="dropdown-item" href="/?id_cat=<?php echo $categoria->Id; ?>"><?php echo $categoria->Nombre; ?></a>   
                                    <?php endforeach; ?>                                    
                                </div>
                            </li>                            
                            <li class="nav-item"><a class="nav-link scrollto" href="/#contactar">Contactar</a></li>
                        </ul>
                        <div class="extras">
                            <?php if($this->model->utilidades->checkLogin()): ?>
                            <a href="/admin" class="mr-1 pr-3 pl-3">Panel</a><a href="/usuario/editar" class="mr-1 pr-3 pl-3">Editar perfil</a><a href="/salir" class="pr-3 pl-3">Cerrar sesión</a>
                            <?php else: ?>
                            <a href="/usuario" class="mr-1 pr-3 pl-3">Regístrate</a><a href="/usuario/login" class="pr-3 pl-3">Iniciar sesión</a>
                            <?php endif; ?>                            
                        </div>
                        
                    </div>

                </nav>
                
            </div>
        </header>        

        <div class="container">
            <div class="row">
                <div class="col text-center">
                    
                    <div class="mb-3"><img src="/assets/img/news.svg" alt="" width="65" /></div>

                    

                    <?php if($this->model->page == 'home'): ?>

                    <?php if(isset($_GET['keyword'])): ?>
                    
                    <div class="hero__micro-title mb-2">Búsqueda por clave</div>
                    <div class="hero__title mb-1"><strong><?php echo $this->model->utilidades->sanitize($_GET['keyword']); ?></strong></div>
                    
                    <?php elseif (isset($_GET['id_cat'])): ?>

                    <div class="hero__micro-title mb-2">Filtrado por categoría</div>
                    <div class="hero__title mb-1"><?php echo ($_GET['id_cat'] > 0) ? $this->articulo->obtenerNombreCategoria($_GET['id_cat']) : 'Todos'; ?></div>

                    <?php else: ?>                    

                    <?php foreach($this->model->listar_ultimo_articulo($this->model->id_cat) as $articulo): ?>
                    <div class="hero__micro-title mb-2">Último articulo publicado</div>
                    <div class="hero__title mb-1"><a href="/articulo?id=<?php echo $articulo->Id; ?>"><?php echo $articulo->Titulo; ?></a></div>
                    <div class="hero__meta mb-3"><a href="/?id_cat=<?php echo $articulo->Id_categoria; ?>"><?php echo $this->articulo->obtenerNombreCategoria($articulo->Id_categoria); ?></a> <span>|</span> Autor: <?php echo $this->articulo->obtenerNombreAutor($articulo->Id_usuario); ?> <span>|</span> <?php echo $articulo->Fecha; ?></div>       
                    <div class="mb-3"><p class="m-0"><?php echo $this->model->utilidades->extracto($articulo->Cuerpo,20); ?></p>                        
                    </div>                    
                    <a href="/articulo?id=<?php echo $articulo->Id; ?>" class="button button--light d-inline-block">Continuar leyendo</a>                    
                    <?php endforeach; ?>                                 
                    
                    <?php endif; ?>
                    
                    <?php elseif ($this->model->page == 'articulo'): ?>
                                        
                    <div class="hero__micro-title mb-2"><?php echo $this->model->obtenerNombreCategoria($articulo->Id_categoria); ?></div>
                    <div class="hero__title mb-1"><?php echo $articulo->Titulo; ?></div>
                    <div class="hero__meta mb-3"><a href="/?id_cat=<?php echo $articulo->Id_categoria; ?>"><?php echo $this->model->obtenerNombreCategoria($articulo->Id_categoria); ?></a> <span>|</span> Autor: <?php echo $this->model->obtenerNombreAutor($articulo->Id_usuario); ?> <span>|</span> <?php echo $articulo->Fecha; ?></div>    
                    
                    <?php elseif ($this->model->page == 'admin'): ?>
                    
                    <div class="hero__micro-title mb-2"><?php echo $this->model->subtitle; ?></div>
                    <div class="hero__title mb-4"><?php echo $this->model->title; ?></div>                    
                                        
                    <?php if(Utilidades::checkLogin() && Utilidades::tipoUsuario($_SESSION['id']) == 'Autor') : ?>
                    <a href="/admin/crear" class="button button--light d-inline-block">Añadir un nuevo articulo</a>
                    <?php endif; ?>

                    <?php else: ?>                    

                    <div class="hero__micro-title mb-2"><?php echo $this->model->subtitle; ?></div>
                    <div class="hero__title mb-1"><?php echo $this->model->title; ?></div>
                    
                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>