<?php
// var_dump($articulo);
?>
    
    <article class="p-5 articulo">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-10">                    
                    <?php if(!empty($articulo->Imagen)) : ?>
                    <div class="articulo__image mb-4"><img src="<?php echo $articulo->Imagen; ?>" alt="<?php echo $this->model->obtenerNombreCategoria($articulo->Id_categoria); ?>" class="img-full" /></div>
                    <?php endif; ?>
                    <div class="articulo__body mb-4">
                        <?php echo $articulo->Cuerpo; ?>
                    </div>     
                    <div class="articulo__keywords"><?php echo $articulo->Palabras_clave; ?></div>
                </div>
            </div>            
        </div>        
    </article>    

    <?php if(!$this->model->utilidades->checkLogin()): ?>

    <div class="container">
        <div class="row">
            <div class="col">
                <hr class="mt-0"/>
            </div>
        </div>
    </div>

    <section class="p-5">
            <div class="container">
                <div class="row">
                    <div class="col text-center">                                                                                          
                        <h2 class="decorate text-center mb-5">¿Te gustaría publicar los artículos <strong>personalizados</strong>?</h2>                            
                        <p class="mb-5">¡Hola! Os invitamos a crear vuestra cuenta personal y empezar a publicar el contenido y las noticias personales. Os asignamos un editar que os ayudara en principio de vuestro camino como escritor digital.</p>          
                        <a href="/usuario" class="button scrollto">Regístrate</a>
                    </div>
                </div>            
            </div>        
    </section>    
    <?php endif; ?>

    