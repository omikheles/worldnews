<?php

require_once PATH.'/models/articulo.php';
require_once PATH.'/models/home.php';

class HomeController
{

    private $model;
    public $utilidades;
    public $groupMsg;
    public $id_cat;
    public $articulo;
    public $keyword;

    /**
     * Constructor de contralador Home
     */
    public function __construct(){
        $this->model = new Home();
        $this->articulo = new Articulo;
        $this->utilidades = new Utilidades();

        $this->model->id_cat = !empty($_GET['id_cat']) ? $this->utilidades->sanitize($_GET['id_cat']) : null;
        $this->model->keyword = !empty($_GET['keyword']) ? $this->utilidades->sanitize($_GET['keyword']) : null;
    }

    /**
     * Metodo por defecto
     */
    public function Index(){
        require_once HEADER;

        if(isset($_GET['keyword'])){
            require_once PATH.'/views/home/home.buscar.php';
        } else {
            require_once PATH.'/views/home/home.php';
        }
        
        require_once FOOTER;
    }   

    /**
     * Buscar por clave 
     */
    public function buscar_clave(){
        $id_keyword = $this->model->obtenerPalabraClave($this->model->keyword) ? $this->model->obtenerPalabraClave($this->model->keyword)->Id : null;        
        $articulos_por_keyword = $this->model->obtenerArticulosPorPalabra($id_keyword) ? implode(",",$this->model->obtenerArticulosPorPalabra($id_keyword)) : null;
        return !empty($articulos_por_keyword) ? $this->model->listar_articulos_in($articulos_por_keyword) : null;  
    }
    
}