<?php

require_once PATH.'/models/404.php';

class ErrorController
{
    private $model;

    public function __construct(){
        $this->model = new Error404();        
    }

    public function Index(){        
        require_once HEADER;
        require_once PATH.'/views/404.php';
        require_once FOOTER;
    }
        
}