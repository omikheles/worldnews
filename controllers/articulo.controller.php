<?php

require_once PATH.'/models/articulo.php';

class ArticuloController
{

    private $model;

    public $utilidades;
    public $id;

    /**
     * Constructor Articulo
     */
    public function __construct(){
        
        $this->model = new Articulo();
        $this->utilidades = new Utilidades();

        $this->model->id = !empty($_GET['id']) ? $this->utilidades->sanitize($_GET['id']) : 0;        

        // Asignamos los valores de campos segun el submit de registro, perfil o login
        if(isset($_POST['registrar']) || isset($_POST['editar'])){
            $this->model->id = !empty($_POST['id']) ? $_POST['id'] : 0;
            $this->model->nombre = !empty($_POST['nombre']) ? $_POST['nombre'] : null;
            $this->model->apellidos = !empty($_POST['apellidos']) ? $_POST['apellidos'] : null;
            $this->model->movil = !empty($_POST['movil']) ? $_POST['movil'] : null;
            $this->model->email = !empty($_POST['email']) ? $_POST['email'] : null;
            $this->model->nombre_usuario = !empty($_POST['nombre-usuario']) ? $_POST['nombre-usuario'] : null;
            $this->model->fecha_nacimiento = !empty($_POST['fecha-nacimiento']) ? $_POST['fecha-nacimiento'] : null;
            $this->model->contrasena = !empty($_POST['contrasena']) ? md5($_POST['contrasena']) : null;
            $this->model->contrasena2 = !empty($_POST['contrasena2']) ? md5($_POST['contrasena2']) : null;
        } elseif(isset($_POST['login'])){
            $this->model->nombre_usuario = !empty($_POST['nombre-usuario']) ? $_POST['nombre-usuario'] : null;
            $this->model->contrasena = !empty($_POST['contrasena']) ? md5($_POST['contrasena']) : null;
        }

    }

    /**
     * El articulo suelto
     */
    public function Index(){                
        
        // Recuperamos el objeto de articulo
        if($articulo = $this->model->obtenerArticulo($this->model->id)){
            // articulo recuperado
        } else {
            header('Location: /');
        }
        require_once HEADER;
        require_once PATH.'/views/articulo/articulo.php';
        require_once FOOTER;
    }        
    
}