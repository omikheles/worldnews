<?php

require_once PATH.'/models/usuario.php';

class UsuarioController
{

    private $model;
    public $utilidades;

    /**
     * Constructor usuario
     */
    public function __construct(){
        
        $this->model = new Usuario();
        $this->utilidades = new Utilidades();                

        // Asignamos los valores de campos segun el submit de registro, perfil o login
        if(isset($_POST['registrar']) || isset($_POST['editar'])){
            $this->model->id = !empty($_POST['id']) ? $_POST['id'] : 0;
            $this->model->nombre = !empty($_POST['nombre']) ? $_POST['nombre'] : null;
            $this->model->apellidos = !empty($_POST['apellidos']) ? $_POST['apellidos'] : null;
            $this->model->movil = !empty($_POST['movil']) ? $_POST['movil'] : null;
            $this->model->email = !empty($_POST['email']) ? $_POST['email'] : null;
            $this->model->nombre_usuario = !empty($_POST['nombre-usuario']) ? $_POST['nombre-usuario'] : null;
            $this->model->fecha_nacimiento = !empty($_POST['fecha-nacimiento']) ? date('Y-m-d', strtotime($_POST['fecha-nacimiento'])) : null;
            $this->model->contrasena = !empty($_POST['contrasena']) ? md5($_POST['contrasena']) : null;
            $this->model->contrasena2 = !empty($_POST['contrasena2']) ? md5($_POST['contrasena2']) : null;
        } elseif(isset($_POST['login'])){
            $this->model->nombre_usuario = !empty($_POST['nombre-usuario']) ? $_POST['nombre-usuario'] : null;
            $this->model->contrasena = !empty($_POST['contrasena']) ? md5($_POST['contrasena']) : null;
        }

    }

    /**
     * El formulario de registro
     */
    public function Index(){

        if($this->utilidades->checkLogin()){
            header('Location: /admin');
        }        

        if(isset($_POST['registrar']) && $this->model->registrar($this->model)){
            $_SESSION['login'] = $this->model->email;
            $_SESSION['id'] = $this->model->id;
            header('Location: /usuario#registrado');            
        }

        require_once HEADER;
        require_once PATH.'/views/usuario/registro.php';
        require_once FOOTER;
    }
        
    /**
     * El formulario de editar perfil. Antes de mostrar comprobamos si el usuario esta autorizado y recuperamos el objeto desde la base.
     */
    public function editar(){
        
        if($this->utilidades->checkLogin()){
            $user = $this->model->obtener($_SESSION['id']);
        } else {
            header('Location: /usuario/login');
        }

        if(isset($_POST['editar']) && $this->model->actualizar($this->model)){
            header('Location: /usuario/editar#actualizado');        
        }
                
        require_once HEADER;
        require_once PATH.'/views/usuario/editar.php';
        require_once FOOTER;
    }

    /**
     * El formulario de entrada
     */
    public function login(){

        if($this->utilidades->checkLogin()){
            header('Location: /admin');
        }
        
        if(isset($_POST['login'])){
                        
            $username = $this->model->nombre_usuario;
            $password = $this->model->contrasena;
            
            if($rs = $this->model->login($username,$password)){
                                
                $_SESSION['login'] = $username;
                $_SESSION['id'] = $rs['Id'];

                header('Location: /admin');                               
                                
            } else {
                $this->model->errors = 'Usuario no existe. Asegura que el e-mail y contraseña son correctos.';
            }
        }

        require_once HEADER;
        require_once PATH.'/views/usuario/login.php';
        require_once FOOTER;
    }
    
}