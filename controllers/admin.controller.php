<?php

require_once PATH.'/models/admin.php';

class AdminController
{

    private $model;
    public $utilidades;

    /**
     * Constructor de Admin
     */
    public function __construct(){
        
        $this->model = new Admin();
        $this->utilidades = new Utilidades();

        if(isset($_POST['crear']) || isset($_POST['editar'])){
            $this->model->titulo_articulo = !empty($_POST['titulo_articulo']) ? $_POST['titulo_articulo'] : null;
            $this->model->cuerpo_articulo = !empty($_POST['cuerpo_articulo']) ? $_POST['cuerpo_articulo'] : null;        
            $this->model->palabras_clave_articulo = !empty($_POST['palabras_clave_articulo']) ? trim(preg_replace('/\s+/',' ', strtolower($_POST['palabras_clave_articulo']))) : null;            
            $this->model->categoria_articulo = !empty($_POST['categoria_articulo']) ? $_POST['categoria_articulo'] : null;
        }

    }

    /**
     * Panel de usuario
     */
    public function Index(){

        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }
                     
        require_once HEADER;
        require_once PATH.'/views/admin/admin.php';
        require_once FOOTER;
    }

    /**
     * Crear articulo nuevo
     */
    public function crear()
    {
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }       

        if(isset($_POST['crear']) && $this->model->crear($this->model)){
            header('Location: /admin');
        }

        require_once HEADER;
        require_once PATH.'/views/admin/crear.php';
        require_once FOOTER;
    }

    /**
     * Eliminar artículo
     */
    public function eliminar()
    {
        
        if(!$this->utilidades->checkLogin()){
            header('Location: /usuario/login');
        }

        $id = !empty($_GET['id']) ? Utilidades::sanitize($_GET['id']) : null;        
                        
        if($this->model->eliminar($id,$this->model->id_usuario)){
            header('Location: /admin#eliminado');
        }        

    }

    /**
     * Editar artículo
     */
    public function editar()
    {
        
        if($this->utilidades->checkLogin()){

            if(empty($_GET['id'])){
                header('Location: /admin');    
            }

            $id = !empty($_GET['id']) ? Utilidades::sanitize($_GET['id']) : null;
            
            $this->model->id_articulo = $id;            

            if(!$articulo = $this->model->obtenerArticulo($id)){
                header('Location: /admin');
            }

        } else {
            header('Location: /usuario/login');
        }
        
        if(isset($_POST['editar']) && $this->model->actualizar($this->model)){
            // var_dump($this->model);
            header('Location: /admin/editar?id='.$this->model->id_articulo.'#actualizado');        
        }

        require_once HEADER;
        require_once PATH.'/views/admin/editar.php';
        require_once FOOTER;
    }
    
}