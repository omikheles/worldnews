$(function(){
   
    $('.scrollto').on('click',function(){
        var el = $(this).attr('href');
        $([document.documentElement, document.body]).animate({
            scrollTop: $(el).offset().top
        }, 1500);        
    });

    $('#trigger-search').on('click',function(e){
        e.preventDefault();
        $('#category-filter').toggleClass('d-none');
    });

    /**
     * Redirección para select de categoría
     */
    $('select[name=filtro-categoria]').on('change', function() {                
        window.location.href = '/?id_cat='+this.value;
    });

    (function(){
        if(!$('#cuerpo_articulo').length)return;

        ClassicEditor
            .create(document.querySelector( '#cuerpo_articulo' ),{
                toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ]
            })
            .catch( error => {
                console.error( error );
            });

    })();
    
    /**
     * Formulario de registro
     */
    (function(){
        
        var $form = $('.register-form'),
            $errors_container = $('.errors');

        // Validamos si las contraseñas coinciden
        $form.on('submit',function(e){
                    
            $errors_container.html('');

            if($('input[name=contrasena]',this).val() !== $('input[name=contrasena2]',this).val()){
                $errors_container.html('Las contraseñas no coinciden.');
                e.preventDefault();
            }
            
        });

    })();

    /**
     * Formulario de contacto
     */
    (function(){
        
        var form = $('form[id=contact]');

        form.on('submit',function(e){

            e.preventDefault();            

            $.ajax({
                type: 'POST',
                url: "/contactar",
                data: form.serialize(),
                cache: false,
                success: function(data){                        
                },
                statusCode: {
                    200: function(){
                        form.addClass('d-none');
                        $('.result').removeClass('d-none');
                    },
                    404: function(){
                        console.log('something is wrong');
                    }
                }
            });

        });

    })();
    

});