const gulp          = require('gulp');
const sass          = require('gulp-sass');
const autoprefixer  = require('autoprefixer');
const postcss       = require('gulp-postcss');

const supportedBrowsers = [
    'last 3 versions' // http://browserl.ist/?q=last+3+versions
];

const autoprefixConfig = { browsers: supportedBrowsers, cascade: false };

// sass
gulp.task('sass', function(){
    var plugins = [        
        autoprefixer(autoprefixConfig)      
    ];
    return gulp.src('./public_html/assets/sass/**/*.sass')    
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(postcss(plugins))
        .pipe(gulp.dest('./public_html/assets/css'));
});

// Watch
gulp.task('watch',function(){    
    gulp.watch('./public_html/assets/sass/**/*.sass',gulp.parallel('sass'));
});

gulp.task('default', gulp.series('watch'));