<?php

class Utilidades
{
    
    /**
     * Terminar la sesión
     */
    public function session_end(){      
        session_destroy();        
    }

    /**
     * Obtener el nombre de la pagina
     */
    public function getPageName(){
        $routes = explode('/',strtok($_SERVER["REQUEST_URI"],'?'));
        $routes = !empty($routes[1]) ? $routes[1] : null;
        return strtolower($routes);
    }

    /**
     * Obtener la accion de pagina. El segundo nivel de la URL
     */
    public function getPageAction(){
        $routes = explode('/',strtok($_SERVER["REQUEST_URI"],'?'));
        $routes = !empty($routes[2]) ? $routes[2] : null;
        return strtolower($routes);
    }

    /**
     * Comprobamos si el usuario esta autorizado
     */
    public function checkLogin(){
        if (isset($_SESSION['login']) && isset($_SESSION['id'])) {
            return true;
        }
    }

    /**
     * Conseguimos el nombre de tipo de usuario autorizado
     * @param int $id El ID de usuario
     */
    public function tipoUsuario($id){
        $pdo = Database::StartUp();
        try 
		{
            
            $stm = "SELECT Id_rol FROM usuario WHERE Id = ? LIMIT 1";
            $stm = $pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                  
                $id_rol = $stm->fetchColumn();
                $stm = "SELECT Nombre FROM rol WHERE Id = ? LIMIT 1";
                $stm = $pdo->prepare($stm);
                $stm->execute(array($id_rol));
            }                        

            return $stm->fetchColumn();

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

    public function nombreUsuario($id){
        $pdo = Database::StartUp();
        try 
		{
            
            $stm = "SELECT Nombre FROM usuario WHERE Id = ? LIMIT 1";
            $stm = $pdo->prepare($stm);
            $stm->execute(array($id));

            if($stm->rowCount() > 0){                  
                return $stm->fetchColumn();                
            }

        } catch (Exception $e) {
            $this->errors = $e->getMessage();            
		}
    }

    /**
     * Limpiamos cualquier valor
     */
    public function sanitize($field)
    {        
        $field = strip_tags($field);
        $field = trim($field);
        $field = stripslashes($field);
        return $field = htmlspecialchars($field);                        
    }

    /**
     * Listar todas las categorias disponibles
     */
    public function listarCategorias()
    {
        $pdo = Database::StartUp();
        try
		{            
			$stm = $pdo->prepare("SELECT * FROM categoria ORDER BY Nombre ASC");
            $stm->execute();            
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{            
            $this->errors = $e->getMessage();
        }
    }

    public function extracto($text,$limit)
    {           
        $text = strip_tags($text);        

        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }

        return $text;

    }

}